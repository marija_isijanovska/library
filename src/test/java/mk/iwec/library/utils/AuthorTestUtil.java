package mk.iwec.library.utils;

import mk.iwec.library.domain.Author;
import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.AuthorPojo;

public class AuthorTestUtil {
	
	public static AuthorPojo createMockAuthorPojo(String firstname, String lastname) {

		AuthorPojo mock = new AuthorPojo();
		mock.setFirstname(firstname);
		mock.setLastname(lastname);
		return mock;

	}
	
	public static Author createMockAuthorEntity() {

		Author mock = new Author();
		mock.setFirstname("Mock firstname");
		mock.setLastname("Mock lastname");
		
		return mock;
	}

}
