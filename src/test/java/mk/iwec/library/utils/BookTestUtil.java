package mk.iwec.library.utils;


import java.sql.Date;

import mk.iwec.library.domain.Book;
import mk.iwec.library.dto.BookPojo;

public class BookTestUtil {
	
	public static Book createMockBockEntity() {
		Book mock = new Book();
		mock.setTitle("Mock title");
		mock.setDescription("Mock description");
		mock.setEdition("Mock edition");
		mock.setCategory("Mock category");
		mock.setFormat("Mock format");
		mock.setPublishdate(new Date(2021,04,04));

		return mock;
	}

	public static BookPojo createMockBookPojo(String title, String description, String edition, String category,
			String format, Date publishdate) {

		BookPojo mock = new BookPojo();
		mock.setTitle(title);
		mock.setDescription(description);
		mock.setEdition(edition);
		mock.setCategory(category);
		mock.setFormat(format);
		mock.setPublishdate(publishdate);
		return mock;
	}

}
