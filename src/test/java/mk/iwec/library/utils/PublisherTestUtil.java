package mk.iwec.library.utils;

import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.PublisherPojo;

public class PublisherTestUtil {
	
	public static Publisher createMockPublisherEntity() {

		Publisher mock = new Publisher();
		mock.setName("Mock name");
		return mock;
	}

	public static PublisherPojo createMockPublisherPojo(String name) {

		PublisherPojo mock = new PublisherPojo();
		mock.setName(name);
		return mock;

	}

}
