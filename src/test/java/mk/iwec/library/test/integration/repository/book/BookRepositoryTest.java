package mk.iwec.library.test.integration.repository.book;

import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Author;
import mk.iwec.library.domain.Book;
import mk.iwec.library.repository.BookRepository;
import mk.iwec.library.utils.BookTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j
public class BookRepositoryTest {
	@Autowired
	private BookRepository bookRepository;
	
	@Test
	public void injectedComponentsAreNotNull() {
		assertThat(bookRepository).isNotNull();
	}

	@Test
	public void getAllBooks() {
		Book mock = BookTestUtil.createMockBockEntity();
		bookRepository.save(mock);

		List<Book> bookList = bookRepository.findAll();
		assertThat(bookList).isNotEmpty();
	}

	@Test
	public void createNewBook() {
		Book mock = BookTestUtil.createMockBockEntity();
		bookRepository.save(mock);

		Optional<Book> persistedOptionalMock = bookRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		Book persistedMock = persistedOptionalMock.get();

		assertThat(persistedMock.getTitle()).isEqualTo(mock.getTitle());
		assertThat(persistedMock.getDescription()).isEqualTo(mock.getDescription());
		assertThat(persistedMock.getCategory()).isEqualTo(mock.getCategory());
		assertThat(persistedMock.getEdition()).isEqualTo(mock.getEdition());
		assertThat(persistedMock.getFormat()).isEqualTo(mock.getFormat());
		assertThat(persistedMock.getPublishdate()).isEqualTo(mock.getPublishdate());

	}

	@Test
	public void updateBook() {
		Book mock = BookTestUtil.createMockBockEntity();
		bookRepository.save(mock);
		Date publishdate = new Date(2021,04,04);

		Optional<Book> persistedOptionalMock = bookRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		assertThat(persistedOptionalMock.get().getTitle()).isEqualTo(mock.getTitle());
		assertThat(persistedOptionalMock.get().getDescription()).isEqualTo(mock.getDescription());
		assertThat(persistedOptionalMock.get().getCategory()).isEqualTo(mock.getCategory());
		assertThat(persistedOptionalMock.get().getEdition()).isEqualTo(mock.getEdition());
		assertThat(persistedOptionalMock.get().getFormat()).isEqualTo(mock.getFormat());
		assertThat(persistedOptionalMock.get().getPublishdate()).isEqualTo(mock.getPublishdate());
		// update book and verify
		Book mockForUpdate = persistedOptionalMock.get();
		mockForUpdate.setTitle("Updated Mock Title");
		mockForUpdate.setDescription("Updated Mock Description");
		mockForUpdate.setCategory("Updated Mock Category");
		mockForUpdate.setEdition("Updated Mock Edition");
		mockForUpdate.setFormat("Updated Mock Format");
		mockForUpdate.setPublishdate(publishdate);
		

		bookRepository.saveAndFlush(mockForUpdate);

		Optional<Book> persistedUpdatedOptionalMock = bookRepository.findById((mockForUpdate.getId()));
		assertThat(persistedUpdatedOptionalMock).isPresent();

		Book persistedUpdatedMock = persistedUpdatedOptionalMock.get();

		assertThat(persistedUpdatedMock.getId().equals(mockForUpdate.getId()));
		assertThat(persistedUpdatedMock.getTitle()).isEqualTo(mockForUpdate.getTitle());
		assertThat(persistedUpdatedMock.getDescription()).isEqualTo(mockForUpdate.getDescription());
		assertThat(persistedUpdatedMock.getCategory()).isEqualTo(mockForUpdate.getCategory());
		assertThat(persistedUpdatedMock.getFormat()).isEqualTo(mockForUpdate.getFormat());
		assertThat(persistedUpdatedMock.getEdition()).isEqualTo(mockForUpdate.getEdition());
		assertThat(persistedUpdatedMock.getPublishdate()).isEqualTo(mockForUpdate.getPublishdate());

	}

	@Test
	public void removeBook() {
		// create book and verify
		Book mock = BookTestUtil.createMockBockEntity();
		bookRepository.save(mock);

		Optional<Book> persistedOptionalMock = bookRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		Book persistedMock = persistedOptionalMock.get();

		assertThat(persistedMock.getId().equals(mock.getId()));
		assertThat(persistedMock.getTitle()).isEqualTo(mock.getTitle());
		assertThat(persistedMock.getDescription()).isEqualTo(mock.getDescription());
		assertThat(persistedMock.getCategory()).isEqualTo(mock.getCategory());
		assertThat(persistedMock.getFormat()).isEqualTo(mock.getFormat());
		assertThat(persistedMock.getEdition()).isEqualTo(mock.getEdition());
		assertThat(persistedMock.getPublishdate()).isEqualTo(mock.getPublishdate());

		// remove book
		bookRepository.delete(persistedMock);
		Optional<Book> deletedOptionalMock = bookRepository.findById(mock.getId());
		assertThat(deletedOptionalMock).isNotPresent();
	}
	
}
