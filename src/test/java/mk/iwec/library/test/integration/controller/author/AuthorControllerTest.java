package mk.iwec.library.test.integration.controller.author;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.Assert.fail;

import com.fasterxml.jackson.databind.ObjectMapper;

import mk.iwec.library.dto.AuthorPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.AuthorServiceImpl;
import mk.iwec.library.utils.AuthorTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.jpa.hibernate.ddl-auto=none")
@AutoConfigureMockMvc

public class AuthorControllerTest {
	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	@MockBean
	AuthorServiceImpl authorService;

	@Test
	public void injectedComponentsAreNotNull() {
		assertThat(mockMvc).isNotNull();
		assertThat(objectMapper).isNotNull();
	}

	@Test
	public void getAllBooksTest() {
		List<AuthorPojo> mockBookPojoList = new ArrayList<>();
		AuthorPojo mock1 = AuthorTestUtil.createMockAuthorPojo("mock firstname 1", "mock lastname 1");
		AuthorPojo mock2 = AuthorTestUtil.createMockAuthorPojo("mock firstname 2", "mock lastname 2");
		
		mockBookPojoList.add(mock1);
		mockBookPojoList.add(mock2);
		
		when(this.authorService.getAll()).thenReturn(mockBookPojoList);

		try {
			this.mockMvc.perform(get(Endpoints.AUTHORS))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].firstname").value(mock1.getFirstname()))
				.andExpect(jsonPath("$[0].lastname").value(mock1.getLastname()))
				.andExpect(jsonPath("$[1].firstname").value(mock2.getFirstname()))
			    .andExpect(jsonPath("$[1].lastname").value(mock2.getLastname()));

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void createBookTest() {
		AuthorPojo mock = AuthorTestUtil.createMockAuthorPojo("mock firstname 2","mock lastname 2");
		AuthorPojo createdMock = AuthorTestUtil.createMockAuthorPojo("mock firstname 2","mock lastname 2");
		
		when(this.authorService.create(mock)).thenReturn(createdMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(post(Endpoints.BOOKS)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.firstname").value(createdMock.getFirstname()))
				.andExpect(jsonPath("$.lastname").value(createdMock.getLastname()));

				
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void updateBook() {
		Long mockId = 1L;
		AuthorPojo mock = AuthorTestUtil.createMockAuthorPojo("mock firstname 1","mock lastname 1");
		AuthorPojo updatedMock = AuthorTestUtil.createMockAuthorPojo("mock firstname 1","mock lastname 1");
		
		when(this.authorService.update(mockId, mock)).thenReturn(updatedMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(put(Endpoints.BOOKS + mockId)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.firstname").value(updatedMock.getFirstname()))
				.andExpect(jsonPath("$.lastname").value(updatedMock.getLastname()));

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
