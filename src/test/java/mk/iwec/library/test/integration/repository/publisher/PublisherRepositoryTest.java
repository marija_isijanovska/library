package mk.iwec.library.test.integration.repository.publisher;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Publisher;
import mk.iwec.library.repository.PublisherRepository;
import mk.iwec.library.utils.PublisherTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j

public class PublisherRepositoryTest {
	
		@Autowired
		private PublisherRepository publisherRepository;
		@Test
		public void injectedComponentsAreNotNull() {
			assertThat(publisherRepository).isNotNull();
		}

		@Test
		public void getAllPublishers() {
			Publisher mock = PublisherTestUtil.createMockPublisherEntity();
			publisherRepository.save(mock);

			List<Publisher> publishers = publisherRepository.findAll();
			assertThat(publishers).isNotEmpty();
		}

		@Test
		public void createNewPublisher() {
			Publisher mock = PublisherTestUtil.createMockPublisherEntity();
			publisherRepository.save(mock);

			Optional<Publisher> persistedOptionalMock = publisherRepository.findById(mock.getId());
			assertThat(persistedOptionalMock).isPresent();
			Publisher persistedMock = persistedOptionalMock.get();

			assertThat(persistedMock.getName()).isEqualTo(mock.getName());

		}

		@Test
		public void updatePublisher() {
			Publisher mock = PublisherTestUtil.createMockPublisherEntity();
			publisherRepository.save(mock);

			Optional<Publisher> persistedOptionalMock = publisherRepository.findById(mock.getId());
			assertThat(persistedOptionalMock).isPresent();
			assertThat(persistedOptionalMock.get().getName()).isEqualTo(mock.getName());
			Publisher mockForUpdate = persistedOptionalMock.get();
			mockForUpdate.setName("Updated Mock name");

			publisherRepository.saveAndFlush(mockForUpdate);

			Optional<Publisher> persistedUpdatedOptionalMock = publisherRepository.findById((mockForUpdate.getId()));
			assertThat(persistedUpdatedOptionalMock).isPresent();

			Publisher persistedUpdatedMock = persistedUpdatedOptionalMock.get();

			assertThat(persistedUpdatedMock.getId().equals(mockForUpdate.getId()));
			assertThat(persistedUpdatedMock.getName()).isEqualTo(mock.getName());

		}

		@Test
		public void removePublisher() {
			Publisher mock = PublisherTestUtil.createMockPublisherEntity();
			publisherRepository.save(mock);

			Optional<Publisher> persistedOptionalMock = publisherRepository.findById(mock.getId());
			assertThat(persistedOptionalMock).isPresent();
			Publisher persistedMock = persistedOptionalMock.get();

			assertThat(persistedMock.getId().equals(mock.getId()));
			assertThat(persistedMock.getName()).isEqualTo(mock.getName());

			publisherRepository.delete(persistedMock);
			Optional<Publisher> deletedOptionalMock = publisherRepository.findById(mock.getId());
			assertThat(deletedOptionalMock).isNotPresent();
		}

	}