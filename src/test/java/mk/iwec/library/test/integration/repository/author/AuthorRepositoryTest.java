package mk.iwec.library.test.integration.repository.author;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Author;
import mk.iwec.library.repository.AuthorRepository;
import mk.iwec.library.utils.AuthorTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Slf4j

public class AuthorRepositoryTest {
	
	@Autowired
	private AuthorRepository authorRepository;
	@Test
	public void injectedComponentsAreNotNull() {
		assertThat(authorRepository).isNotNull();
	}

	@Test
	public void getAllAuthors() {
		Author mock = AuthorTestUtil.createMockAuthorEntity();
		authorRepository.save(mock);

		List<Author> authors = authorRepository.findAll();
		assertThat(authors).isNotEmpty();
	}

	@Test
	public void createNewAuthor() {
		Author mock = AuthorTestUtil.createMockAuthorEntity();
		authorRepository.save(mock);

		Optional<Author> persistedOptionalMock = authorRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		Author persistedMock = persistedOptionalMock.get();

		assertThat(persistedMock.getFirstname()).isEqualTo(mock.getFirstname());
		assertThat(persistedMock.getLastname()).isEqualTo(mock.getLastname());

	}

	@Test
	public void updateAuthor() {
		Author mock = AuthorTestUtil.createMockAuthorEntity();
		authorRepository.save(mock);

		Optional<Author> persistedOptionalMock = authorRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		assertThat(persistedOptionalMock.get().getFirstname()).isEqualTo(mock.getFirstname());
		assertThat(persistedOptionalMock.get().getLastname()).isEqualTo(mock.getLastname());
		Author mockForUpdate = persistedOptionalMock.get();
		mockForUpdate.setFirstname("Updated Mock Firstname");
		mockForUpdate.setLastname("Updated Mock Lastname");

		authorRepository.saveAndFlush(mockForUpdate);

		Optional<Author> persistedUpdatedOptionalMock = authorRepository.findById((mockForUpdate.getId()));
		assertThat(persistedUpdatedOptionalMock).isPresent();

		Author persistedUpdatedMock = persistedUpdatedOptionalMock.get();

		assertThat(persistedUpdatedMock.getId().equals(mockForUpdate.getId()));
		assertThat(persistedUpdatedMock.getFirstname()).isEqualTo(mock.getFirstname());
		assertThat(persistedUpdatedMock.getLastname()).isEqualTo(mock.getLastname());

	}

	@Test
	public void removeAuthor() {
		Author mock = AuthorTestUtil.createMockAuthorEntity();
		authorRepository.save(mock);

		Optional<Author> persistedOptionalMock = authorRepository.findById(mock.getId());
		assertThat(persistedOptionalMock).isPresent();
		Author persistedMock = persistedOptionalMock.get();

		assertThat(persistedMock.getId().equals(mock.getId()));
		assertThat(persistedMock.getFirstname()).isEqualTo(mock.getFirstname());
		assertThat(persistedMock.getLastname()).isEqualTo(mock.getLastname());

		authorRepository.delete(persistedMock);
		Optional<Author> deletedOptionalMock = authorRepository.findById(mock.getId());
		assertThat(deletedOptionalMock).isNotPresent();
	}

}
