package mk.iwec.library.test.integration.controller.book;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.Assert.fail;

import com.fasterxml.jackson.databind.ObjectMapper;

import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.BookServiceImpl;
import mk.iwec.library.utils.BookTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.jpa.hibernate.ddl-auto=none")
@AutoConfigureMockMvc
public class BookControllerTest {
	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	@MockBean
	BookServiceImpl bookService;

	@Test
	public void injectedComponentsAreNotNull() {
		assertThat(mockMvc).isNotNull();
		assertThat(objectMapper).isNotNull();
	}

	@Test
	public void getAllBooksTest() {
		List<BookPojo> mockBookPojoList = new ArrayList<>();
		BookPojo mock1 = BookTestUtil.createMockBookPojo("mock title 1", "mock description 1","mock edition 1","mock format 1","mock category 1",new Date(2021,04,04));
		BookPojo mock2 = BookTestUtil.createMockBookPojo("mock title 2", "mock description 2","mock edition 2","mock format 2","mock category 2",new Date(2021,04,04));
		
		mockBookPojoList.add(mock1);
		mockBookPojoList.add(mock2);
		
		when(this.bookService.getAll()).thenReturn(mockBookPojoList);

		try {
			this.mockMvc.perform(get(Endpoints.BOOKS))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].title").value(mock1.getTitle()))
				.andExpect(jsonPath("$[0].description").value(mock1.getDescription()))
				.andExpect(jsonPath("$[0].edition").value(mock1.getEdition()))
				.andExpect(jsonPath("$[0].format").value(mock1.getFormat()))
				.andExpect(jsonPath("$[0].category").value(mock1.getCategory()))
				.andExpect(jsonPath("$[0].publishdate").value(mock1.getPublishdate()))
				.andExpect(jsonPath("$[1].title").value(mock2.getTitle()))
			    .andExpect(jsonPath("$[1].description").value(mock2.getDescription()))
			    .andExpect(jsonPath("$[1].edition").value(mock2.getEdition()))
			    .andExpect(jsonPath("$[1].format").value(mock2.getFormat()))
			    .andExpect(jsonPath("$[1].category").value(mock2.getCategory()))
			    .andExpect(jsonPath("$[1].publishdate").value(mock2.getPublishdate()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void createBookTest() {
		BookPojo mock = BookTestUtil.createMockBookPojo("mock title 1", "mock description 1","mock edition 1","mock format 1","mock category 1",new Date(2021,04,04));
		BookPojo createdMock = BookTestUtil.createMockBookPojo("mock title 1", "mock description 1","mock edition 1","mock format 1","mock category 1",new Date(2021,04,04));
		
		when(this.bookService.create(mock)).thenReturn(createdMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(post(Endpoints.BOOKS)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.title").value(createdMock.getTitle()))
				.andExpect(jsonPath("$.description").value(createdMock.getDescription()))
				.andExpect(jsonPath("$.edition").value(createdMock.getDescription()))
				.andExpect(jsonPath("$.format").value(createdMock.getFormat()))
				.andExpect(jsonPath("$.category").value(createdMock.getCategory()))
				.andExpect(jsonPath("$.publishdate").value(createdMock.getPublishdate()));
				
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void updateBook() {
		Long mockId = 1L;
		BookPojo mock = BookTestUtil.createMockBookPojo("mock title 1", "mock description 1","mock edition 1","mock format 1","mock category 1",new Date(2021,04,04));
		BookPojo updatedMock = BookTestUtil.createMockBookPojo("mock title 1", "mock description 1","mock edition 1","mock format 1","mock category 1",new Date(2021,04,04));
		
		when(this.bookService.update(mockId, mock)).thenReturn(updatedMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(put(Endpoints.BOOKS + mockId)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.title").value(updatedMock.getTitle()))
				.andExpect(jsonPath("$.description").value(updatedMock.getDescription()))
				.andExpect(jsonPath("$.edition").value(updatedMock.getDescription()))
				.andExpect(jsonPath("$.format").value(updatedMock.getFormat()))
				.andExpect(jsonPath("$.category").value(updatedMock.getCategory()))
				.andExpect(jsonPath("$.publishdate").value(updatedMock.getPublishdate()));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
