package mk.iwec.library.test.integration.controller.publisher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.Assert.fail;

import com.fasterxml.jackson.databind.ObjectMapper;

import mk.iwec.library.dto.PublisherPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.PublisherServiceImpl;
import mk.iwec.library.utils.PublisherTestUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.jpa.hibernate.ddl-auto=none")
@AutoConfigureMockMvc

public class PublisherControlerTest {
	
	@Autowired
	protected MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	@MockBean
	PublisherServiceImpl publisherService;

	@Test
	public void injectedComponentsAreNotNull() {
		assertThat(mockMvc).isNotNull();
		assertThat(objectMapper).isNotNull();
	}

	@Test
	public void getAllBooksTest() {
		List<PublisherPojo> mockBookPojoList = new ArrayList<>();
		PublisherPojo mock1 = PublisherTestUtil.createMockPublisherPojo("mock name 1");
		PublisherPojo mock2 = PublisherTestUtil.createMockPublisherPojo("mock name 2");
		
		mockBookPojoList.add(mock1);
		mockBookPojoList.add(mock2);
		
		when(this.publisherService.getAll()).thenReturn(mockBookPojoList);

		try {
			this.mockMvc.perform(get(Endpoints.AUTHORS))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].name").value(mock1.getName()))
			    .andExpect(jsonPath("$[1].name").value(mock2.getName()));

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void createBookTest() {
		PublisherPojo mock = PublisherTestUtil.createMockPublisherPojo("mock name 1");
		PublisherPojo createdMock = PublisherTestUtil.createMockPublisherPojo("mock name 2");
		
		when(this.publisherService.create(mock)).thenReturn(createdMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(post(Endpoints.BOOKS)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name").value(createdMock.getName()));

				
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void updateBook() {
		Long mockId = 1L;
		PublisherPojo mock = PublisherTestUtil.createMockPublisherPojo("mock name 1");
		PublisherPojo updatedMock = PublisherTestUtil.createMockPublisherPojo("mock name 1");
		
		when(this.publisherService.update(mockId, mock)).thenReturn(updatedMock);

		try {
			String jsonBodyPayload = objectMapper.writer().writeValueAsString(mock);
			
			this.mockMvc.perform(put(Endpoints.PUBLISHERS + mockId)
					.contentType(MediaType.APPLICATION_JSON)
					.content(jsonBodyPayload))
				.andDo(print())
				.andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name").value(updatedMock.getName()));

		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}


}
