import axios from 'axios'
const PUBLISHERS_URL ='http://localhost:3000/api/publishers/';

class PublisherService{
 
    getPublishers(){
       return axios.get(PUBLISHERS_URL);
     }

     createPublisher(publisher){
       return axios.post(PUBLISHERS_URL,publisher);
     }

     getPublisherById(publisherId){
       return axios.get(PUBLISHERS_URL+ '/' + publisherId);

     }

     updatePublisher(publisher,publisherId){
       return axios.put(PUBLISHERS_URL+ '/' + publisherId,publisher);
     }

     deletePublisher(publisherId){
       return axios.delete(PUBLISHERS_URL+ '/' + publisherId);
     }
}
export default  new PublisherService();