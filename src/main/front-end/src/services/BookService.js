import axios from 'axios'
const BOOKS_URL ='http://localhost:3000/api/books/';

class BookService{
 
    getBooks(){
       return axios.get(BOOKS_URL);
     }

     createBook(book){
       return axios.post(BOOKS_URL,book);
     }

     getBookById(bookId){
       return axios.get(BOOKS_URL+ '/' + bookId);

     }

     updateBook(book,bookId){
       return axios.put(BOOKS_URL+ '/' + bookId,book);
     }

     deleteBook(bookId){
       return axios.delete(BOOKS_URL+ '/' + bookId);
     }
}
export default new BookService();