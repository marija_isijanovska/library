import axios from 'axios'
const AUTHORS_URL ='http://localhost:3000/api/authors/';

class AuthorService{
 
    getAuthors(){
       return axios.get(AUTHORS_URL);
     }

     createAuthor(author){
       return axios.post(AUTHORS_URL,author);
     }

     getAuthorById(authorId){
       return axios.get(AUTHORS_URL+ '/' + authorId);

     }

     updateAuthor(author,authorId){
       return axios.put(AUTHORS_URL+ '/' + authorId,author);
     }

     deleteAuthor(authorId){
       return axios.delete(AUTHORS_URL+ '/' + authorId);
     }
}
export default new AuthorService();