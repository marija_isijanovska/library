import React from 'react';
import AuthorService from '../services/AuthorService';

class ListAuthorComponent extends React.Component{

             constructor(props){
              super(props)
              this.state={
               authors:[]
            }
            this.addAuthor = this.addAuthor.bind(this);
            this.editAuthor = this.editAuthor.bind(this);
            this.deleteAuthor = this.deleteAuthor.bind(this);
            this.handleBack = this.handleBack.bind(this);
            
         }


             componentDidMount(){

              AuthorService.getAuthors().then((response) => {
              this.setState({authors: response.data})
            })
            .catch((error) => {
                console.log(error);
              });

            }

            deleteAuthor(id){
               AuthorService.deleteAuthor(id).then(response => {
                this.setState({authors: this.state.authors.filter(author => author.id !== id)});
               });

            }

            addAuthor(){
               this.props.history.push('/add-author');

            }
            editAuthor(id){
               this.props.history.push('/update-author/'+id);

            }

            handleBack(){
               this.props.history.push('/');
            }

    render(){

 return (
    
 <div>
    <h1 className="text-center"> Authors page </h1>
    <div className="row">
       <button style={{ marginLeft: "20px", width: "120px"}} className="btn btn-primary"  onClick = {this.addAuthor}> Add author </button>
       <button style={{ width: "100px"}} onClick={this.handleBack} className="btn btn-link">Back</button>
    </div>
    <table style={{ textAlign: "center" }} className="table table-striped">
       <thead>

          <tr>
          
           <th>Id</th>
           <th>Firstname</th>
           <th>Lastname</th>
          </tr>
       </thead>
      <tbody>
{
         this.state.authors.map(

           author => 
            <tr key={author.id}>
               <td>{author.id}</td>
               <td>{author.firstname}</td>
               <td>{author.lastname}</td>
        
               <td>
                  <button style={{ width: "100px"}} onClick= { () => this.editAuthor(author.id)} className="btn btn-info">Update</button>
                  <button style={{ width: "100px"}} onClick= { () => this.deleteAuthor(author.id)} className="btn btn-danger">Delete</button>
               </td>


            </tr>
            
         )
    }
      </tbody>
      

       </table>

 </div>
 )
}
}

export default ListAuthorComponent;