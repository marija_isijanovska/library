import React, {Component} from 'react'
import BookService from '../services/BookService';
class UpdateBookComponent extends Component{
 constructor(props){
  super(props)
  
  this.changeTitleHandler = this.changeTitleHandler.bind(this);
  this.changeDescriptionHandler = this.changeDescriptionHandler.bind(this);
  this.changeCategoryHandler = this.changeCategoryHandler.bind(this);
  this.changeFormatHandler = this.changeFormatHandler.bind(this);
  this.changeEditionHandler = this.changeEditionHandler.bind(this);
  this.changePublishdateHandler = this.changePublishdateHandler.bind(this);
  this.changeAuthorFirstNameHandler = this.changeAuthorFirstNameHandler.bind(this);
  this.changeAuthorLastNameHandler = this.changeAuthorLastNameHandler.bind(this);
  this.changePublisherHandler = this.changePublisherHandler.bind(this);
  this.updateBook = this.updateBook.bind(this);
  
  this.state={
   id: this.props.match.params.id,
   title:'',
   description: '',
   category:'',
   format:'',
   edition: '',
   publishdate:'',
   authors:[{
     firstname:'',
     lastname:''
   }
   ],
   publisher:{
     name: ''
   }
  }
  }
  
componentDidMount(){
 BookService.getBookById(this.state.id).then( (response) =>{
    let book = response.data;
    this.setState({
     title: book.title,
     description: book.description,
     category: book.category,
     format: book.format,
     edition: book.edition,
     publishdate: book.publishdate,
     authors: book.authors[0],
     publisher: book.publisher
    });
 });

}

  updateBook = (event) => {
   event.preventDefault();
  
        let authorsObj = Object.assign({}, this.state.authors);
        let dataObj = [{ firstname: authorsObj.firstname, lastname: authorsObj.lastname }];
          let book = {
            title: this.state.title,
            description: this.state.description, 
            category: this.state.category, 
            format: this.state.format, 
            edition: this.state.edition, 
            publishdate: this.state.publishdate, 
            authors: dataObj,
            publisher: this.state.publisher,
          }
  
   console.log('book => ' + JSON.stringify(book));
   BookService.updateBook(book,this.state.id).then(response => {
    this.props.history.push('/books');
   });
  }
  
  cancel(){
    this.props.history.push('/books');
  }
  changeTitleHandler= (event) => { 
   this.setState({title:event.target.value});
  }
  
  changeDescriptionHandler= (event) => { 
   this.setState({description:event.target.value});
  }
  
  changeCategoryHandler= (event) => { 
   this.setState({category:event.target.value});
  }
  
  changeFormatHandler= (event) => { 
   this.setState({format:event.target.value});
  }
  
  changeEditionHandler= (event) => { 
   this.setState({edition:event.target.value});
  }
  
  changePublishdateHandler= (event) => { 
   this.setState({publishdate:event.target.value});
  }
  
  changeAuthorFirstNameHandler(event) {
    this.setState(prevState => {
        let authors = Object.assign({}, prevState.authors);
        authors.firstname = event.target.value;
        return { authors };
    })
  }
  
  changeAuthorLastNameHandler(event) {
  
    this.setState(prevState => {
        let authors = Object.assign({}, prevState.authors);
        authors.lastname = event.target.value;
        return { authors };
    })
  }
  
  changePublisherHandler= (event) => { 
    this.setState(prevState => ({
      publisher: {
          ...prevState.publisher,
          name: event.target.value
      }
  }));
  }
  
  render() {
  
   return (
  
    <div>
     <div className="App">
       <div className="row">
         <div className="card col-md-6 offset-md-3 offset-md-3">
           <h3 className="text-center"> Update book</h3>
         <div className="card-body">
      <form>
       <div className="form-group ">
        <label >title</label>
        <input placeholder="Title" name="title" className="form-control"
        value={this.state.title} onChange={this.changeTitleHandler} />
       </div>
      
       <div className="form-group ">
        <label>description</label>
        <input placeholder="Description" name="description" className="form-control"
        value={this.state.description} onChange={this.changeDescriptionHandler} />
       </div>
  
       <div className="form-group ">
        <label>category</label>
        <input placeholder="Category" name="category" className="form-control"
        value={this.state.category} onChange={this.changeCategoryHandler} />
       </div>
  
       <div className="form-group ">
        <label>edition</label>
        <input placeholder="Edition" name="edition" className="form-control"
        value={this.state.edition} onChange={this.changeEditionHandler} />
       </div>
  
       <div className="form-group ">
        <label>format</label>
        <input placeholder="Format" name="format" className="form-control"
        value={this.state.format} onChange={this.changeFormatHandler} />
       </div>
  
       <div className="form-group ">
        <label>publishdate</label>
        <input placeholder="Publish date" name="publishdate" className="form-control"
        value={this.state.publishdate} onChange={this.changePublishdateHandler} />
       </div>
  
       <div className="form-group">
      <label>author firstname </label>
      <input placeholder="Author firstname" name="firstname" className="form-control"
       value={this.state.authors.firstname} onChange={this.changeAuthorFirstNameHandler}/>
       </div>
       
       <div className="form-group">
       <label>author lastname </label>
       <input placeholer="Author lastname" name ="lastname" className="form-control"
       value={this.state.authors.lastname} onChange={this.changeAuthorLastNameHandler}/>
       </div>
  
       <div className="form-group ">
        <label>publisher</label>
        <input placeholder="Publisher" name="publisher" className="form-control"
        value={this.state.publisher.name} onChange={this.changePublisherHandler} />
       </div>
  
  <button className="btn btn-success" onClick={this.updateBook}>Save</button>
  <button className="btn btn-danger" onClick={this.cancel.bind(this)} style = {{marginLeft : "10px" }}> Cancel </button>
      </form>
      </div>
     </div>
     </div>
     </div>
    </div>
   )
  }
}

export default UpdateBookComponent



