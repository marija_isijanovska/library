import React from 'react';
import BookService from '../services/BookService';

class ListBookComponent extends React.Component{

             constructor(props){
              super(props)
              this.state={
               books:[]
            }
            this.addBook = this.addBook.bind(this);
            this.editBook = this.editBook.bind(this);
            this.deleteBook = this.deleteBook.bind(this);
            this.handleBack = this.handleBack.bind(this);
            
         }


             componentDidMount(){

              BookService.getBooks().then((response) => {
              this.setState({books: response.data})
            })
            .catch((error) => {
                console.log(error);
              });

            }

            deleteBook(id){
               BookService.deleteBook(id).then(response => {
                this.setState({books: this.state.books.filter(book => book.id !== id)});
               });

            }

            addBook(){
               this.props.history.push('/add-book');

            }
            editBook(id){
               this.props.history.push('/update-book/'+id);

            }

            handleBack(){
               this.props.history.push('/');
            }

    render(){

 return (
    
 <div>
    <h1 className="text-center"> Books page </h1>
    <div className="row">
       <button style={{ marginLeft: "20px", width: "120px"}} className="btn btn-primary"  onClick = {this.addBook}> Add book </button>
       <button style={{ width: "100px"}} onClick={this.handleBack} className="btn btn-link">Back</button>
    </div>
    <table style={{ textAlign: "center" }} className="table table-striped">
       <thead>

          <tr>
          
           <th>Id</th>
           <th>Title</th>
           <th>Description</th>
           <th>Category</th>
           <th>Format</th>
           <th>Edition</th>
           <th>Publish date</th>
           <th>Authors</th>
           <th>Publisher</th>
           <th>Actions</th>
           
           
           
          </tr>
       </thead>
      <tbody>
{
         this.state.books.map(

            book => 
            <tr key={book.id}>
               <td>{book.id}</td>
               <td>{book.title}</td>
               <td>{book.description}</td>
               <td>{book.category}</td>
               <td>{book.format}</td>
               <td>{book.edition}</td>
               <td>{book.publishdate}</td>
               <td>{book.authors[0].firstname.concat(" ",book.authors[0].lastname)}</td>
               <td>{book.publisher.name}</td>
               <td>
                  <button style={{ width: "100px"}} onClick= { () => this.editBook(book.id)} className="btn btn-info">Update</button>
                  <button style={{ width: "100px"}} onClick= { () => this.deleteBook(book.id)} className="btn btn-danger">Delete</button>
               </td>


            </tr>
            
         )
    }
      </tbody>
      

       </table>

 </div>
 )
}
}

export default ListBookComponent;