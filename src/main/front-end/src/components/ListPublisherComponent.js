import React from 'react';
import PublisherService from '../services/PublisherService';

class ListPublisherComponent extends React.Component{

             constructor(props){
              super(props)
              this.state={
               publishers:[]
            }
            this.addPublisher = this.addPublisher.bind(this);
            this.editPublisher = this.editPublisher.bind(this);
            this.deletePublisher = this.deletePublisher.bind(this);
            this.handleBack = this.handleBack.bind(this);
            
         }


             componentDidMount(){

              PublisherService.getPublishers().then((response) => {
              this.setState({publishers: response.data})
            })
            .catch((error) => {
                console.log(error);
              });

            }

            deletePublisher(id){
               PublisherService.deletePublisher(id).then(response => {
                this.setState({publishers: this.state.publishers.filter(publisher => publisher.id !== id)});
               });

            }

            addPublisher(){
               this.props.history.push('/add-publisher');

            }
            editPublisher(id){
               this.props.history.push('/update-publisher/'+id);

            }
            handleBack(){
               this.props.history.push('/');
            }

    render(){

 return (
    
 <div>
    <h1 className="text-center"> Publishers page </h1>
    <div className="row">
       <button style={{ marginLeft: "20px", width: "120px"}} className="btn btn-primary"  onClick = {this.addPublisher}> Add publisher </button>
       <button style={{ width: "100px"}} onClick={this.handleBack} className="btn btn-link">Back</button>
    </div>
    <table style={{ textAlign: "center" }} className="table table-striped">
       <thead>

          <tr>
          
           <th>Id</th>
           <th>Name</th>
          </tr>
       </thead>
      <tbody>
   {
      this.state.publishers.map(

         publisher => 
 <tr key={publisher.id}>
    <td>{publisher.id}</td>
    <td>{publisher.name}</td>

    <td>
       <button style={{ width: "100px"}} onClick= { () => this.editPublisher(publisher.id)} className="btn btn-info">Update</button>
       <button style={{ width: "100px"}} onClick= { () => this.deletePublisher(publisher.id)} className="btn btn-danger">Delete</button>
    </td>


 </tr>
 
)
}
      </tbody>
      

       </table>

 </div>
 )
   }
}

export default ListPublisherComponent;