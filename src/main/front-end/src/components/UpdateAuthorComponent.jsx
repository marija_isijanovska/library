import React, {Component} from 'react'
import AuthorService from '../services/AuthorService';
class UpdateAuthorComponent extends Component{
 constructor(props){
  super(props)
  
  this.changeAuthorFirstNameHandler = this.changeAuthorFirstNameHandler.bind(this);
  this.changeAuthorLastNameHandler = this.changeAuthorLastNameHandler.bind(this);

  this.updateAuthor = this.updateAuthor.bind(this);
  
  this.state={
     id: this.props.match.params.id,
     firstname:'',
     lastname:''
   }
  }
  
componentDidMount(){
 AuthorService.getAuthorById(this.state.id).then( (response) =>{
    let author = response.data;
    this.setState({
     firstname: author.firstname,
     lastname: author.lastname
 
    });
 });

}

  updateAuthor = (event) => {
   event.preventDefault();
  
          let author = {
            firstname: this.state.firstname,
            lastname: this.state.lastname, 
            
          }
  
   console.log('author => ' + JSON.stringify(author));
   AuthorService.updateAuthor(author,this.state.id).then(response => {
    this.props.history.push('/authors');
   });
  }
  
  cancel(){
    this.props.history.push('/authors');
  }
  changeAuthorFirstNameHandler(event) {
   this.setState({firstname:event.target.value});
  }
  
  changeAuthorLastNameHandler(event) {
  
   this.setState({lastname:event.target.value});
  
  }
 
  render() {
  
   return (
  
    <div>
     <div className="App">
       <div className="row">
         <div className="card col-md-6 offset-md-3 offset-md-3">
           <h3 className="text-center"> Update author</h3>
         <div className="card-body">
      <form>

       <div className="form-group">
      <label>Firstname </label>
      <input name="firstname" className="form-control"
       value={this.state.firstname} onChange={this.changeAuthorFirstNameHandler}/>
       </div>
       
       <div className="form-group">
       <label>Lastname </label>
       <input  name ="lastname" className="form-control"
       value={this.state.lastname} onChange={this.changeAuthorLastNameHandler}/>
       </div>
  

  <button className="btn btn-success" onClick={this.updateAuthor}>Save</button>
  <button className="btn btn-danger" onClick={this.cancel.bind(this)} style = {{marginLeft : "10px" }}> Cancel </button>
      </form>
      </div>
     </div>
     </div>
     </div>
    </div>
   )
  }
}

export default UpdateAuthorComponent



