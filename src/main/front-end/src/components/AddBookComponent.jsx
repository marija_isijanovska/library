import React, {Component} from 'react'
import BookService from '../services/BookService';

class AddBookComponent extends Component{
constructor(props){
super(props)

this.changeTitleHandler = this.changeTitleHandler.bind(this);
this.changeDescriptionHandler = this.changeDescriptionHandler.bind(this);
this.changeCategoryHandler = this.changeCategoryHandler.bind(this);
this.changeFormatHandler = this.changeFormatHandler.bind(this);
this.changeEditionHandler = this.changeEditionHandler.bind(this);
this.changePublishdateHandler = this.changePublishdateHandler.bind(this);
this.changeAuthorFirstNameHandler = this.changeAuthorFirstNameHandler.bind(this);
this.changeAuthorLastNameHandler = this.changeAuthorLastNameHandler.bind(this);
this.changePublisherHandler = this.changePublisherHandler.bind(this);
this.saveBook = this.saveBook.bind(this);

this.state={
 title:'',
 description: '',
 category:'',
 format:'',
 edition: '',
 publishdate:'',
 authors:[{
   firstname:'',
   lastname:''
 }
 ],
 publisher:{
   name: ''
 }
}
}

saveBook = (event) => {
 event.preventDefault();

      let authorsObj = Object.assign({}, this.state.authors);
      let dataObj = [{ firstname: authorsObj.firstname, lastname: authorsObj.lastname }];
        let book = {
          title: this.state.title,
          description: this.state.description, 
          category: this.state.category, 
          format: this.state.format, 
          edition: this.state.edition, 
          publishdate: this.state.publishdate, 
          authors: dataObj,
          publisher: this.state.publisher,
        }

 console.log('book => ' + JSON.stringify(book));

 BookService.createBook(book).then(response =>{
  this.props.history.push('/books');
 });
}

cancel(){
  this.props.history.push('/books');
}
changeTitleHandler= (event) => { 
 this.setState({title:event.target.value});
}

changeDescriptionHandler= (event) => { 
 this.setState({description:event.target.value});
}

changeCategoryHandler= (event) => { 
 this.setState({category:event.target.value});
}

changeFormatHandler= (event) => { 
 this.setState({format:event.target.value});
}

changeEditionHandler= (event) => { 
 this.setState({edition:event.target.value});
}

changePublishdateHandler= (event) => { 
 this.setState({publishdate:event.target.value});
}

changeAuthorFirstNameHandler(event) {
  this.setState(prevState => {
      let authors = Object.assign({}, prevState.authors);
      authors.firstname = event.target.value;
      return { authors };
  })
}

changeAuthorLastNameHandler(event) {

  this.setState(prevState => {
      let authors = Object.assign({}, prevState.authors);
      authors.lastname = event.target.value;
      return { authors };
  })
}

changePublisherHandler= (event) => { 
  this.setState(prevState => ({
    publisher: {
        ...prevState.publisher,
        name: event.target.value
    }
}));
}

render() {

 return (

  <div>
   <div className="App">
     <div className="row">
       <div className="card col-md-6 offset-md-3 offset-md-3">
         <h3 className="text-center"> Add book</h3>
       <div className="card-body">
    <form>
     <div className="form-group ">
      <label >Title</label>
      <input  name="title" className="form-control"
      value={this.state.title} onChange={this.changeTitleHandler} />
     </div>
    
     <div className="form-group ">
      <label>Description</label>
      <input  name="description" className="form-control"
      value={this.state.description} onChange={this.changeDescriptionHandler} />
     </div>

     <div className="form-group ">
      <label>Category</label>
      <input  name="category" className="form-control"
      value={this.state.category} onChange={this.changeCategoryHandler} />
     </div>

     <div className="form-group ">
      <label>Edition</label>
      <input  name="edition" className="form-control"
      value={this.state.edition} onChange={this.changeEditionHandler} />
     </div>

     <div className="form-group ">
      <label>Format</label>
      <input  name="format" className="form-control"
      value={this.state.format} onChange={this.changeFormatHandler} />
     </div>

     <div className="form-group ">
      <label>Publishdate</label>
      <input  name="publishdate" className="form-control"
      value={this.state.publishdate} onChange={this.changePublishdateHandler} />
     </div>

     <div className="form-group">
    <label>Author firstname </label>
    <input  name="firstname" className="form-control"
     value={this.state.authors.firstname} onChange={this.changeAuthorFirstNameHandler}/>
     </div>
     
     <div className="form-group">
     <label>Author lastname </label>
     <input name ="lastname" className="form-control"
     value={this.state.authors.lastname} onChange={this.changeAuthorLastNameHandler}/>
     </div>

     <div className="form-group ">
      <label>Publisher</label>
      <input name="publisher" className="form-control"
      value={this.state.publisher.name} onChange={this.changePublisherHandler} />
     </div>


<button className="btn btn-success" onClick={this.saveBook}>Add</button>
<button className="btn btn-danger" onClick={this.cancel.bind(this)} style = {{marginLeft : "10px" }}> Cancel </button>
    </form>
    </div>
   </div>
   </div>
   </div>
  </div>
 )
}
}

export default AddBookComponent