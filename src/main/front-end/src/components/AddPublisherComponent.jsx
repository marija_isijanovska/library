import React, {Component} from 'react'
import PublisherService from '../services/PublisherService';

class AddPublisherComponent extends Component{
constructor(props){
super(props)

this.changePublisherFirstNameHandler = this.changePublisherFirstNameHandler.bind(this);

this.state={
   name:'',
 }
}

savePublisher = (event) => {
 event.preventDefault();

        let publisher = {
          name: this.state.name
        }

 console.log('publisher => ' + JSON.stringify(publisher));

 PublisherService.createPublisher(publisher).then(response =>{
  this.props.history.push('/publishers');
 });
}

cancel(){
  this.props.history.push('/publishers');
}

changePublisherFirstNameHandler(event) {
 this.setState({name:event.target.value});
}

render() {

 return (

  <div>
   <div className="App">
     <div className="row">
       <div className="card col-md-6 offset-md-3 offset-md-3">
         <h3 className="text-center"> Add publisher</h3>
       <div className="card-body">
    <form>

     <div className="form-group">
    <label>Name </label>
    <input  name="firstname" className="form-control"
     value={this.state.name} onChange={this.changePublisherFirstNameHandler}/>
     </div>
     
<button className="btn btn-success" onClick={this.savePublisher}>Add</button>
<button className="btn btn-danger" onClick={this.cancel.bind(this)} style = {{marginLeft : "10px" }}> Cancel </button>
    </form>
    </div>
   </div>
   </div>
   </div>
  </div>
 )
}
}

export default AddPublisherComponent