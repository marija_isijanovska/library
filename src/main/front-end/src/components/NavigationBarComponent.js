import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class NavigationBarComponent extends Component {

    render() {
        return (
            <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
                <Link to="/" className="navbar-brand">Public library</Link>
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto">
                        <li className="navbar-item">
                            <Link to="/" className="nav-link">Home</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/books" className="nav-link">Books</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/authors" className="nav-link">Authors</Link>
                        </li>
                        <li className="navbar-item">
                            <Link to="/publishers" className="nav-link">Publishers</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}