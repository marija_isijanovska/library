import React from 'react'
import './App.css';
import ListBookComponent from './components/ListBookComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import AddBookComponent from './components/AddBookComponent';
import UpdateBookComponent from './components/UpdateBookComponent';
import ListAuthorComponent from './components/ListAuthorComponent';
import AddAuthorComponent from './components/AddAuthorComponent';
import UpdateAuthorComponent from './components/UpdateAuthorComponent';
import ListPublisherComponent from './components/ListPublisherComponent';
import AddPublisherComponent from './components/AddPublisherComponent';
import UpdatePublisherComponent from './components/UpdatePublisherComponent'
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import NavigationBarComponent from './components/NavigationBarComponent';

function App() {
  return (
    
      <div>
        <Router>
      <HeaderComponent />
      <div className="App">
        <Switch>
          <Route path= "/" exact component={NavigationBarComponent}></Route>
          <Route path= "/books" component={ListBookComponent}></Route>
          <Route path= "/add-book" component={AddBookComponent}></Route>
          <Route path= "/update-book/:id" component={UpdateBookComponent}></Route>

          <Route path= "/authors" component={ListAuthorComponent}></Route>
          <Route path= "/add-author" component={AddAuthorComponent}></Route>
          <Route path= "/update-author/:id" component={UpdateAuthorComponent}></Route>

          <Route path= "/publishers" component={ListPublisherComponent}></Route>
          <Route path= "/add-publisher" component={AddPublisherComponent}></Route>
          <Route path= "/update-publisher/:id" component={UpdatePublisherComponent}></Route>
     
     </Switch>
        </div>
        <FooterComponent/>
        
        </Router>
</div>

  );
}

export default App;
