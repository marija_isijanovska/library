package mk.iwec.library.dto;

import java.sql.Date;
import java.util.List;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import mk.iwec.library.domain.Author;
import mk.iwec.library.domain.Publisher;
import mk.iwec.library.pojo.BasePojo;

@Data
public class BookPojo extends BasePojo{
	
	private String title;
	private String description;
	private String edition;
	private String format;
	private String category;
	private Date publishdate;
	private List<Author> authors;
	private Publisher publisher;
}
