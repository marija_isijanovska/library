package mk.iwec.library.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import mk.iwec.library.domain.Book;
import mk.iwec.library.pojo.BasePojo;

@Data
public class PublisherPojo extends BasePojo {
	
	private String name;
	private List<Book> books;
	
	@JsonIgnoreProperties(value = {"publisher"})
	public List<Book> getBooks(){
		return books;
	}
	
	
}
