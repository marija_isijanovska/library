package mk.iwec.library.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import mk.iwec.library.domain.Book;
import mk.iwec.library.pojo.BasePojo;

@Data
public class AuthorPojo extends BasePojo {
	
	private String firstname;
	private String lastname;
    private List<Book> books;
    
    @JsonIgnoreProperties(value = {"authors"})
	public List<Book> getBooks(){
		return books;
	}
}
