package mk.iwec.library.mapper;

import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.PublisherPojo;
import mk.iwec.library.infrastructure.mapper.GeneralMapper;

public interface PublisherMapper extends GeneralMapper<PublisherPojo,Publisher> {
	
	public void mapRequestedFieldForUpdate(Publisher entity, PublisherPojo dto);

}
