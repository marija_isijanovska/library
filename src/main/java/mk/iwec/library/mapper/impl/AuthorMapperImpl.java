package mk.iwec.library.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mk.iwec.library.domain.Author;
import mk.iwec.library.dto.AuthorPojo;
import mk.iwec.library.infrastructure.mapper.AbstractGeneralMapper;
import mk.iwec.library.mapper.AuthorMapper;
@Component
public class AuthorMapperImpl extends AbstractGeneralMapper implements AuthorMapper {

	@Autowired
	public AuthorMapperImpl(ModelMapper modelMapper) {
		super(modelMapper);
	}

	@Override
	public AuthorPojo entityToDto(Author author) {
		return this.modelMapper.map(author,AuthorPojo.class);
	}

	@Override
	public Author dtoToEntity(AuthorPojo authorDto) {
		return this.modelMapper.map(authorDto, Author.class);
	}

	@Override
	public void mapRequestedFieldForUpdate(Author entity, AuthorPojo dto) {
		entity.setFirstname(dto.getFirstname());
		entity.setLastname(dto.getLastname());
		entity.setBooks(dto.getBooks());

	}


}
