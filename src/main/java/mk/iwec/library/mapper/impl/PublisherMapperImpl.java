package mk.iwec.library.mapper.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.PublisherPojo;
import mk.iwec.library.infrastructure.mapper.AbstractGeneralMapper;
import mk.iwec.library.mapper.PublisherMapper;
@Component
public class PublisherMapperImpl extends AbstractGeneralMapper implements PublisherMapper {
	@Autowired
	public PublisherMapperImpl(ModelMapper modelMapper) {
		super(modelMapper);
	}

	@Override
	public PublisherPojo entityToDto(Publisher publisher) {
		return this.modelMapper.map(publisher, PublisherPojo.class);
	}

	@Override
	public Publisher dtoToEntity(PublisherPojo publisherDto) {
		return this.modelMapper.map(publisherDto, Publisher.class);
	}

	@Override
	public void mapRequestedFieldForUpdate(Publisher entity, PublisherPojo dto) {
		entity.setName(dto.getName());
		entity.setBooks(dto.getBooks());

	}

}
