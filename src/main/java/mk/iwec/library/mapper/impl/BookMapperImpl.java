package mk.iwec.library.mapper.impl;

import java.sql.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mk.iwec.library.domain.Book;
import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.infrastructure.mapper.AbstractGeneralMapper;
import mk.iwec.library.mapper.BookMaper;
@Component
public class BookMapperImpl extends AbstractGeneralMapper implements BookMaper {

	@Autowired
	public BookMapperImpl(ModelMapper modelMapper) {
		super(modelMapper);
	}

	@Override
	public BookPojo entityToDto(Book book) {
		return this.modelMapper.map(book, BookPojo.class);
	}

	@Override
	public Book dtoToEntity(BookPojo bookDto) {
		return this.modelMapper.map(bookDto, Book.class);
	}

	@Override
	public void mapRequestedFieldForUpdate(Book entity, BookPojo dto) {

		entity.setTitle(dto.getTitle());
		entity.setDescription(dto.getDescription());
		entity.setEdition(dto.getEdition());
		entity.setFormat(dto.getFormat());
		entity.setCategory(dto.getCategory());
		entity.setPublishdate(dto.getPublishdate());
		entity.setAuthors(dto.getAuthors());
		entity.setPublisher(dto.getPublisher());
		
	}

}
