package mk.iwec.library.mapper;

import mk.iwec.library.domain.Author;
import mk.iwec.library.dto.AuthorPojo;
import mk.iwec.library.infrastructure.mapper.GeneralMapper;

public interface AuthorMapper extends GeneralMapper<AuthorPojo,Author>{
	
	public void mapRequestedFieldForUpdate(Author entity, AuthorPojo dto);

}
