package mk.iwec.library.mapper;

import mk.iwec.library.domain.Book;
import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.infrastructure.mapper.GeneralMapper;

public interface BookMaper extends GeneralMapper<BookPojo,Book > {
	
	public void mapRequestedFieldForUpdate(Book entity, BookPojo dto);

}
