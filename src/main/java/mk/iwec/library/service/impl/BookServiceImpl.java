package mk.iwec.library.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Book;
import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.infrastructure.exceptions.ResourceNotFoundException;
import mk.iwec.library.mapper.BookMaper;
import mk.iwec.library.repository.BookRepository;
import mk.iwec.library.service.GenericService;

@Service
@Slf4j
@Transactional
public class BookServiceImpl implements GenericService<BookPojo, Long> {
	
	@Autowired
	public BookRepository bookRepository;
	@Autowired
	public BookMaper bookMapper;

	@Override
	public BookPojo findById(Long id) {
		Book bookEntity = bookRepository.findById(id).orElseThrow(() -> {
			log.error("Resource Book with id {} is not found", id);
			return new ResourceNotFoundException("Resource Book not found");
		});

		return bookMapper.entityToDto(bookEntity);
	}

	@Override
	public List<BookPojo> getAll() {
		log.debug("Execute getAll Book");
		return bookMapper.mapList(bookRepository.findAll(), BookPojo.class);
	}

	@Override
	public BookPojo create(BookPojo bookDto) {
		log.debug("Execute create Book with parameters ", bookDto);
		Book transientBook = bookMapper.dtoToEntity(bookDto);
		Book persistedBook = bookRepository.save(transientBook);

		return bookMapper.entityToDto(persistedBook);
	}

	@Override
	public BookPojo update(Long id, BookPojo bookDto) {
		log.debug("Execute update Book with parameters {}", bookDto);
		Book persistedEntity = bookMapper.dtoToEntity(findById(id));
        bookMapper.mapRequestedFieldForUpdate(persistedEntity, bookDto);
		return bookMapper.entityToDto(bookRepository.saveAndFlush(persistedEntity));
	}

	@Override
	public void remove(Long id) {
		log.debug("Execute deleteById Book with parameters {}", id);
		bookRepository.deleteById(id);
	}

}
