package mk.iwec.library.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Author;
import mk.iwec.library.dto.AuthorPojo;
import mk.iwec.library.infrastructure.exceptions.ResourceNotFoundException;
import mk.iwec.library.mapper.AuthorMapper;
import mk.iwec.library.repository.AuthorRepository;
import mk.iwec.library.service.GenericService;

@Service
@Slf4j
@Transactional
public class AuthorServiceImpl implements GenericService<AuthorPojo, Long> {

	@Autowired 
	public AuthorRepository authorRepository;
	@Autowired
	public AuthorMapper authorMapper;
	
	@Override
	public AuthorPojo findById(Long id) {
		Author authorEntity = authorRepository.findById(id).orElseThrow(() -> {
			log.error("Resource Author with id {} is not found", id);
			return new ResourceNotFoundException("Resource Author not found");
		});

		return authorMapper.entityToDto(authorEntity);
	}

	@Override
	public List<AuthorPojo> getAll() {
		log.debug("Execute findAll Author");
		return authorMapper.mapList(authorRepository.findAll(), AuthorPojo.class);
	}

	@Override
	public AuthorPojo create(AuthorPojo authorDto) {
		log.debug("Execute create Author with parameters ", authorDto);
		Author transientEntity = authorMapper.dtoToEntity(authorDto);
		Author persistedEntity = authorRepository.save(transientEntity);
		return authorMapper.entityToDto(persistedEntity);
	}

	@Override
	public AuthorPojo update(Long id, AuthorPojo authorDto) {
		log.debug("Execute update Author with parameters {}", authorDto);
		Author persistedEntity = authorMapper.dtoToEntity(findById(id));
		authorMapper.mapRequestedFieldForUpdate(persistedEntity, authorDto);
		
		return authorMapper.entityToDto(authorRepository.saveAndFlush(persistedEntity));
	}

	@Override
	public void remove(Long id) {
		log.debug("Execute deleteById Author with parameters {}", id);
		authorRepository.deleteById(id);
	}

}
