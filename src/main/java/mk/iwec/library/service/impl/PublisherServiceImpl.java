package mk.iwec.library.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.library.domain.Author;
import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.dto.PublisherPojo;
import mk.iwec.library.infrastructure.exceptions.ResourceNotFoundException;
import mk.iwec.library.mapper.PublisherMapper;
import mk.iwec.library.repository.PublisherRepository;
import mk.iwec.library.service.GenericService;
@Service
@Slf4j
@Transactional
public class PublisherServiceImpl implements GenericService<PublisherPojo, Long> {
	@Autowired 
	public PublisherRepository publisherRepository;
	@Autowired
	public PublisherMapper publisherMapper;
	
	@Override
	public PublisherPojo findById(Long id) {
		Publisher publisherEntity = publisherRepository.findById(id).orElseThrow(() -> {
			log.error("Resource Publisher with id {} is not found", id);
			return new ResourceNotFoundException("Resource Publisher not found");
		});

		return publisherMapper.entityToDto(publisherEntity);
	}

	@Override
	public List<PublisherPojo> getAll() {
		log.debug("Execute findAll Publisher");
		return publisherMapper.mapList(publisherRepository.findAll(), PublisherPojo.class);
	}

	@Override
	public PublisherPojo create(PublisherPojo publisherDto) {
		log.debug("Execute create Publisher with parameters ", publisherDto);
		Publisher transientEntity = publisherMapper.dtoToEntity(publisherDto);
		Publisher persistedEntity = publisherRepository.save(transientEntity);
		return publisherMapper.entityToDto(persistedEntity);
	}

	@Override
	public PublisherPojo update(Long id, PublisherPojo publisherDto) {
		log.debug("Execute update Publisher with parameters {}",publisherDto);
		Publisher persistedEntity = publisherMapper.dtoToEntity(findById(id));
		 publisherMapper.mapRequestedFieldForUpdate(persistedEntity, publisherDto);
		 
		return publisherMapper.entityToDto(publisherRepository.saveAndFlush(persistedEntity));
	}

	@Override
	public void remove(Long id) {
		log.debug("Execute deleteById Publisher with parameters {}", id);
		publisherRepository.deleteById(id);
	}

}
