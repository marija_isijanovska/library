package mk.iwec.library.service;

import java.util.List;

import org.springframework.stereotype.Service;

import mk.iwec.library.domain.Book;
@Service
public interface GenericService <T,ID>{
	
	public T findById(ID id);
	
	public List<T> getAll();
	
	public T create(T entity);
	
	public T update(ID id, T entity);
	
	public void remove(ID id);

}
