package mk.iwec.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mk.iwec.library.domain.Publisher;
@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long>,CrudRepository<Publisher,Long> {

}
