package mk.iwec.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mk.iwec.library.domain.Author;
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>,CrudRepository<Author,Long> {

}
