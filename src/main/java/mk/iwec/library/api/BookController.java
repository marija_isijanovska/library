package mk.iwec.library.api;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.library.domain.Book;
import mk.iwec.library.dto.BookPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.BookServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping(Endpoints.BOOKS)
public class BookController {
	
	@Autowired
	private BookServiceImpl service;
	
	@GetMapping("/{id}")
	public BookPojo findById(@PathVariable (value = "id") Long id) {
		
		return service.findById(id);
	}
	
	@GetMapping(produces = "application/json")
	@ResponseBody
	public List<BookPojo> findAll() {
		
		return service.getAll();
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
    public BookPojo createBook(@RequestBody BookPojo requestBookPojo) {
		
		return service.create(requestBookPojo);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public BookPojo updateBook(@PathVariable (value = "id") Long id, @RequestBody BookPojo requestBookPojo) {
		return service.update(id, requestBookPojo);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void removeBook(@PathVariable (value = "id") Long id) {
	    service.remove(id);
	}
}
