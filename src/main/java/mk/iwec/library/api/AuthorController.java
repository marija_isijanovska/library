package mk.iwec.library.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import mk.iwec.library.domain.Author;
import mk.iwec.library.dto.AuthorPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.AuthorServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping(Endpoints.AUTHORS)
public class AuthorController {
	
	@Autowired
	AuthorServiceImpl service;
	
	@GetMapping("/{id}")
	public AuthorPojo findById(@PathVariable (value = "id") Long id) {
		
		return service.findById(id);
	}
	
	@GetMapping
	
	public List<AuthorPojo> findAll() {
		return service.getAll();
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
    public AuthorPojo createAuthor(@RequestBody AuthorPojo requestAuthorPojo) {
		
		return service.create(requestAuthorPojo);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public AuthorPojo updateAuthor(@PathVariable (value = "id") Long id, @RequestBody AuthorPojo requestAuthorPojo) {
		return service.update(id, requestAuthorPojo);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void removeAuthor(@PathVariable (value = "id") Long id) {
	    service.remove(id);
	}
}
