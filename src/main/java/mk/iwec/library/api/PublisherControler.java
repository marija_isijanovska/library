package mk.iwec.library.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.library.domain.Publisher;
import mk.iwec.library.dto.PublisherPojo;
import mk.iwec.library.infrastructure.Endpoints;
import mk.iwec.library.service.impl.PublisherServiceImpl;

@CrossOrigin("*")
@RestController
@RequestMapping(Endpoints.PUBLISHERS)
public class PublisherControler {
	
	@Autowired
	PublisherServiceImpl service;
	
	@GetMapping("/{id}")
	public PublisherPojo findById(@PathVariable (value = "id") Long id) {
		
		return service.findById(id);
	}
	
	@GetMapping
	public List<PublisherPojo> findAll() {
		return service.getAll();
	}
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
    public PublisherPojo createPublisher(@RequestBody PublisherPojo requestPublisherPojo) {
		
		return service.create(requestPublisherPojo);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public PublisherPojo updatePublisher(@PathVariable (value = "id") Long id, @RequestBody PublisherPojo requestPublisherPojo) {
		return service.update(id, requestPublisherPojo);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void removePublisher(@PathVariable (value = "id") Long id) {
	    service.remove(id);
	}
}